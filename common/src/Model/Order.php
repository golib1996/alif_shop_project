<?php
include_once __DIR__."/../Service/DBConnector.php";
include_once __DIR__."/../Model/Product.php";

class Order
{
    private $id;
    private $userId;
    private $deliveryId;
    private $paymentId;
    private $total;
    private $status;
    private $created;
    private $updated;
    private $comment;
    private $name;
    private $phone;
    private $email;


    private $conn;

    public function __construct(
        $id = null,
        $userId = null,
        $paymentId = null,
        $deliveryId = null,
        $total = null,
        $name = null,
        $phone = null,
        $email = null,
        $comment = null,
        $status = null,
        $updated = null)
    {
        $this->conn =DBConnector::getInstance()->connect();
        $this->id = $id;
        $this->userId = $userId;
        $this->paymentId = $paymentId;
        $this->deliveryId = $deliveryId;
        $this->total = $total;
        $this->status = $status;
        $this->comment = $comment;
        $this->name = $name;
        $this->phone = $phone;
        $this->email = $email;
        if ($this->id == null) {
            $this->created = date('Y-m-d H:i:s', time());
        }
        $this->updated = $updated ?? date('Y-m-d H:i:s',time());


    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string|null   $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }


    public function setStatus($status) {
        $this->status = $status;
    }

    public function getStatus() {
        return $this->status;
    }

    /**
     * @return int
     */
    public function getDeliveryId()
    {
        return $this->deliveryId;
    }

    /**
     * @param int $deliveryId
     */
    public function setDeliveryId($deliveryId)
    {
        $this->deliveryId = $deliveryId;
    }

    /**
     * @return int
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * @param int $paymentId
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param null $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return false|string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param false|string $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return false|string
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param false|string $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    public function  save(){
            $query = "insert into orders (id, 
                                            user_id,
                                            status,
                                            created,
                                            updated,
                                            delivery_id,
                                            payment_id,
                                            total,
                                            comment,
                                            name,
                                            email,
                                            phone) 
                                                        values(null, 
                                                                    '" . $this->userId . "',
                                                                    '" . $this->status . "',
                                                                    '" . $this->created . "',
                                                                    '" . $this->updated . "',
                                                                    '" . $this->deliveryId . "',
                                                                    '" . $this->paymentId . "',
                                                                    '" . $this->total . "',
                                                                    '" . $this->comment . "',
                                                                    '" . $this->name . "',
                                                                    '" . $this->email . "',
                                                                    '" . $this->phone . "')";

        $result= mysqli_query($this->conn,$query);
        if (!$result) {
            throw new Exception(mysqli_error($this->conn));
        }
        $result = mysqli_query($this->conn,"Select last_insert_id() as last_id");
        $result =  mysqli_fetch_all($result,MYSQLI_ASSOC);
        return reset($result)['last_id'] ?? null;

    }

    public function  update(){
        $query = "update orders set   status = '" . $this->status . "',
                                      updated = '" . $this->updated . "',
                                      delivery_id = '" . $this->deliveryId . "',
                                      payment_id = '" . $this->paymentId . "',
                                      name = '" . $this->name . "',
                                      email = '" . $this->email . "',
                                      phone = '" . $this->phone . "',
                                      total = '" . $this->total . "' where id = '".$this->id."' limit 1";

        $result= mysqli_query($this->conn,$query);
        if (!$result) {
            throw new Exception(mysqli_error($this->conn));
        }
        return true;
    }

    public function getFromDB() {
        $result = mysqli_query($this->conn,"select * from orders where user_id =".$this->userId." limit 1 ");
        $oneProduct = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $oneProduct = reset($oneProduct);
        return $oneProduct;
    }


    public function getById($id) {
        $result = mysqli_query($this->conn,"select * from orders where id =".$id." limit 1 ");
        $one = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $one = reset($one);
        return $one;
    }

    public function getProductsAndQuantityByOrderId($orderId) {

        $products = [];

        $result = mysqli_query($this->conn,"select order_item.quantity, products.* from order_item
                                                  left join products on order_item.product_id=products.id  
                                                  where order_id =".$orderId);

        foreach (mysqli_fetch_all($result,MYSQLI_ASSOC) as $item) {
            $products[] = [
                'quantity'=>$item['quantity'],
                'product'=>new Product($item['id'],$item['title'],$item['picture'],$item['preview'],
                            $item['content'],$item['price'],$item['status'],$item['created'],$item['updated'])
            ];
        }

        return $products;
    }

    public function all() {
        $result = mysqli_query($this->conn,"select * from orders");
        return mysqli_fetch_all($result,MYSQLI_ASSOC);

    }



}