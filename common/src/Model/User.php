<?php
include_once __DIR__."/../Service/DBConnector.php";
include_once __DIR__."/../../../common/src/Service/UserService.php";

class User
{
    const ROLE_USER_VALUE = 'ROLE_USER';
    private $id;
    private $name;
    private $phone;
    private $email;
    private $password;
    private $roles;

    private $conn;

    public function __construct($id = null,$name = null,$phone = null,$email = null,$password = null,$roles = null)
    {
        $this->conn =DBConnector::getInstance()->connect();
        $this->setId($id);
        $this->setName($name);
        $this->setPhone($phone);
        $this->setEmail($email);
        $this->setPassword($password) ;
        $this->setRoles($roles) ;

    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = UserService::encodePassword($password);
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }



    public function save(){
        if ($this->id>0){
            $query = "update `user` set
                                `name` = '" . $this->getName() . "',
                                `phone`='". $this->getPhone() ."',
                                `email`='". $this->getEmail() ."',
                                `password`='". $this->getPassword() ."',
                                `roles`='".json_encode($this->getRoles())  ."',
                               where id='" . $this->id . "' limit 1";
        }else {
            $query = "insert into `user` (`id`,`name`,`phone`,`email`,`password`,`roles`) values(null,
                                                                                            '" . $this->getName() . "',
                                                                                            '" . $this->getPhone() . "',
                                                                                            '" . $this->getEmail() . "',
                                                                                            '" . $this->getPassword() . "',
                                                                                            '" . json_encode($this->getRoles()) . "' )";
        }
        $result =  mysqli_query($this->conn,$query);

        if(!$result) {
            throw new Exception(mysqli_error($this->conn),400);

        }
    }

    public function getById($id) {
        $result = mysqli_query($this->conn,"select * from `user` where id =".$id." limit 1 ");
        $one = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $one = reset($one);
        return $one;
    }

    public function getByEmail($email) {
        $result = mysqli_query($this->conn,"select * from `user` where email = '".$email."' limit 1 ");

        if( !$result) {
            throw new Exception("User not found", 404);
        }


        $one = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $one = reset($one);
        return $one;
    }

    public function isAccess(array $roles, $controller, $action) {

        $permission = SecurityService::getPermissionNameByControllerAndAction($controller,$action);

        $result = mysqli_query($this->conn,"select * from `rbac_access` 
                            where role in  ('". implode("','", $roles) ."') and permission = '$permission'  ");

        if( !$result) {
            throw new Exception("Permission Error", 400);
        }


        $accesses = mysqli_fetch_all($result,MYSQLI_ASSOC);

        foreach ($accesses as $access) {
            if($access) return true;
         }
        throw new Exception("Not Permission", 403);
    }


}