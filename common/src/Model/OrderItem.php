<?php
include_once __DIR__."/../Service/DBConnector.php";
include_once  __DIR__."/../../../common/src/Service/ExceptionService.php";

class OrderItem
{

    public $orderId;
    public $productId;
    public $quantity;


    private $conn;

    public function __construct($orderId = null,$productId = null, $quantity = null)
    {
        $this->conn =DBConnector::getInstance()->connect();
        $this->orderId = $orderId;
        $this->productId = $productId;
        $this->quantity = $quantity;


    }
    public function save(){
        $query = "insert into order_item values(null, '" . $this->orderId . "', '" . $this->productId . "','" . $this->quantity . "')";

        $result= mysqli_query($this->conn,$query);

        if (!$result) {
            throw new Exception(mysqli_error($this->conn));
        }
    }

    public function update(){
        if (empty($this->orderId) || empty($this->productId) || empty($this->quantity)) {
           throw new Exception("Empty order item field");

        }
            $query = "update order_item set quantity = " . $this->quantity . " where order_id = " . $this->orderId
                . " AND product_id = " . $this->productId
                . " LIMIT 1";
        $result= mysqli_query($this->conn,$query);
        if (!$result) {
            throw new Exception(mysqli_error($this->conn));
        }
    }

    public function getByOrderId($orderId) {
        $result = mysqli_query($this->conn,"select * from order_item where order_id = '$orderId'");
        $oneProduct = mysqli_fetch_all($result,MYSQLI_ASSOC);

        return $oneProduct;
    }
    public function deleteProductByOrderId($orderId,$productId){
        mysqli_query($this->conn,"delete from order_item where product_id = '$productId'and order_id = '$orderId' limit 1 ");
    }
    public function ClearByOrderId($orderId){
        mysqli_query($this->conn,"delete from order_item where order_id = '$orderId'");
    }
}