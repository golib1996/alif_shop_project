<?php
include_once __DIR__."/../Service/DBConnector.php";

class Basket
{
    public $id;
    public $userId;
    public $items = [];


    private $conn;

    public function __construct($userId = null)
    {
        $this->conn =DBConnector::getInstance()->connect();
        $this->userId = $userId;


    }
    public function  save(){
            $query = "insert into basket values(null, '" . $this->userId . "')";
        $result= mysqli_query($this->conn,$query);
        if (!$result) {
            throw new Exception(mysqli_error($this->conn));
        }
    }

    public function getFromDB() {
        $result = mysqli_query($this->conn,"select * from basket where user_id =".$this->userId." limit 1 ");
        $oneProduct = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $oneProduct = reset($oneProduct);
        return $oneProduct;
    }
    public function deleteUserById($userId){
        mysqli_query($this->conn,"delete from basket where user_id = '$userId' limit 1 ");
    }


}