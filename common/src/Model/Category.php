<?php
include_once __DIR__."/../Service/DBConnector.php";

class Category
{
    public $id;
    public $title;
    public $groupId;
    public $parentId;
    public $prior;
    private $conn;   


    public function __construct($id = null,$title = null,$groupId = null,$parentId = null, $prior = null)
    {
        $this->conn =DBConnector::getInstance()->connect();
        $this->id = $id;
        $this->title = $title;
        $this->groupId = $groupId;
        $this->parentId = $parentId;
        $this->prior = $prior;
    }

    public function save(){
        if ($this->id>0){
            $query = "update categories set
                                title = '" . $this->title . "',
                                group_id='". $this->groupId ."',
                                parent_id='" . $this->parentId . "',
                        		`prior` = '". $this->prior . "'
                                where id='" . $this->id . "' limit 1";
        }else {
            $query = "insert into categories values(null,
                                                '" . $this->title . "',
                                                '" . $this->groupId . "',
                                                '" . $this->parentId . "',
                                                '" . $this->prior . "'
                                                    )";
        }
        mysqli_query($this->conn,$query);
    }

    public function all() {
        $result = mysqli_query($this->conn,"select * from categories order by id desc");
        return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }

    public function getByGroupIds($groups = []) {
        $where = '';

        if(!empty($groups)) {
            $where = 'WHERE group_id IN ('. implode(',',$groups) . ')';
        }
        $result = mysqli_query($this->conn,"select * from categories $where order by 'prior' desc");

        return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }

    public function getGroupsWithCategories($groups = []) {
        $where = '';

        if(!empty($groups)) {
            $where = 'WHERE group_id not IN ('. implode(',',$groups) . ')';
        }
        $result = mysqli_query($this->conn,"select 
                                                  categories.*,
                                                  cg.id as group_id,
                                                  cg.title as group_title
                                                  from 
                                                  categories 
                                                  left join category_group cg on group_id=cg.id 
                                                  $where order by 'prior' desc");

        $groups = [];
        $categories = mysqli_fetch_all($result,MYSQLI_ASSOC);
        if(!( is_array($categories) && !empty($categories))) {
            return [];
        }

        foreach ($categories as $item) {
            $groups[$item['group_title']][] = $item;
        }

        return $groups;
    }

    public function getById($id) {
        $result = mysqli_query($this->conn,"select * from categories where id = '$id' limit 1 ");
        $one = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $one = reset($one);
        return $one;
    }
    public function deleteById($id){
        mysqli_query($this->conn,"delete from categories where id = '$id' limit 1 ");
    }
}