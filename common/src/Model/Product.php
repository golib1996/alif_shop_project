<?php
include_once __DIR__."/../Service/DBConnector.php";

class Product
{

    const  NUMBER_PRODUCTS_PER_PAGE = 20;

    public $id;
    /**
     * @var string
     * @valid{"type":"string","maxlength": 16}
     */
    public $title;

    public $picture;

    /**
     * @var string
     *  @valid{"type":"string","maxlength": 255}
     */

    public $preview;

    public $content;
    /**
     * @var int
     *  @valid {"type":"int", "max": 30000, "min": 1}
     */

    public  $price;

    /**
     * @var int
     *  @valid {"type":"int"}
     */


    public $status;

    public $created;

    public $updated;

    private $conn;


    public function __construct($id = null,$title = null,$picture = null,$preview = null,$content = null,$price = null,$status = null,$created = null,$updated = null)
    {
        $this->conn =DBConnector::getInstance()->connect();
        $this->id = $id;
        $this->title = $title;
        $this->picture = $picture;
        $this->preview = $preview;
        $this->content = $content;
        $this->price = $price;
        $this->status = $status;
        $this->created = $created;
        $this->updated = $updated;
    }
    public function save(){
        if ($this->id>0){
            $query = "update products set
                                title = '" . $this->title . "',".
                      ((!empty($this->picture)) ? "picture='" . $this->picture. "',": "")
                            ."preview='". $this->preview ."',
                            content='". $this->content."',
                            price='" . $this->price . "',
                            status='" . $this->status . "',
                            updated ='" . $this->updated . "'
                            where id='" . $this->id . "' limit 1";
        }else {
            $query = "insert into products values(null,
                                                '" . $this->title . "',
                                                '" . $this->picture . "',
                                                '" . $this->preview . "',
                                                '" . $this->content . "',
                                                '" . $this->price . "',
                                                '" . $this->status . "',
                                                '" . $this->created  . "',
                                                '" . $this->updated . "')";
        }
        mysqli_query($this->conn,$query);
    }


    public function all($categoryIds = [],$limit = self::NUMBER_PRODUCTS_PER_PAGE, $offset = 20) {

        $where = '';

        if (!empty($categoryIds)) {
            $where = ' WHERE cp.category_id IN ('. implode(',',$categoryIds) . ')';
        }

        $result = mysqli_query($this->conn,"select distinct products.* 
                                                 from 
                                                 products 
                                                 left join category_product cp on products.id = cp.product_id
                                                 $where
                                                 order by id desc limit $offset, $limit");


        return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }

    public function getAllForExport() {

        $result = mysqli_query($this->conn,"select distinct products.* 
                                                 from 
                                                 products order by id");


        return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }

    public function getNumberPage($categoryIds, $limit = self::NUMBER_PRODUCTS_PER_PAGE, $offset = 0)
    {
        if (!empty($categoryIds)) {

            $query = "select  count(*) 
                                from 
                                products 
                                left join category_product cp on products.id = cp.product_id
                                where cp.category_id = $categoryIds ";
        } else
            $query = "select count(*) from products";

        $result = mysqli_query($this->conn, $query);

        $result =  mysqli_fetch_all($result, MYSQLI_ASSOC);
        $number = reset($result);
        $number = reset($number);

        return ceil($number/Product::NUMBER_PRODUCTS_PER_PAGE);
    }


    public function getById($id) {
        $result = mysqli_query($this->conn,"select * from products where id = '$id' limit 1 ");
        $oneProduct = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $oneProduct = reset($oneProduct);
        return $oneProduct;
    }
    public function deleteById($id){
        mysqli_query($this->conn,"delete from products where id = '$id' limit 1 ");
    }
}