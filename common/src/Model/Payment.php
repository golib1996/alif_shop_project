<?php

include_once __DIR__."/../Service/DBConnector.php";

class payment
{
    private $id;
    private $title;
    private $code;
    private $priority;

    private $conn;

    public function __construct($id = null, $title = null, $code = null, $priority = null)
    {
        $this->conn =DBConnector::getInstance()->connect();
        $this->id = $id;
        $this->title = $title;
        $this->code = $code;
        $this->priority = $priority;
    }

    /**
     * @return null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param null $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param null $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return null
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param null $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }

    public function  save(){
        if ($this->id>0){
            $query = "update `payment` set
                                `title` = '" . $this->title . "',
                                `code`='". $this->code ."',
                                `priority`='". $this->priority ."'
                               where id='" . $this->id . "' limit 1";
        }else {
            $query = "insert into payment (id, 
                                            title,
                                            code,
                                            priority) 
                                                        values(null, 
                                                                    '" . $this->title . "',
                                                                    '" . $this->code . "',
                                                                    '" . $this->priority . "')";


        }
        $result= mysqli_query($this->conn,$query);
        if (!$result) {
            throw new Exception(mysqli_error($this->conn));
        }
        $result = mysqli_query($this->conn,"Select last_insert_id() as last_id");
        $result =  mysqli_fetch_all($result,MYSQLI_ASSOC);
        return reset($result)['last_id'] ?? null;
    }


    public function getById($id) {
        $result = mysqli_query($this->conn,"select * from payment where id =".$id." limit 1 ");
        $one = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $one = reset($one);
        return $one;
    }

    public function all() {
        $result = mysqli_query($this->conn,"select * from payment");
        return mysqli_fetch_all($result,MYSQLI_ASSOC);

    }
    public function deleteById($id){
        mysqli_query($this->conn,"delete from payment where id = '$id' limit 1 ");
    }
}