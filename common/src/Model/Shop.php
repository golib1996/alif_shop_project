<?php
include_once __DIR__."/../Service/DBConnector.php";

class Shop
{
    public $id;
    public $title;
    public $address;
    public $city;
    private $conn;


    public function __construct($id = null,$title = null,$address = null,$city = null)
    {
        $this->conn =DBConnector::getInstance()->connect();
        $this->id = $id;
        $this->title = $title;
        $this->address = $address;
        $this->city = $city;

    }
    public function save(){
        if ($this->id>0){
            $query = "update shops set
                                title = '" . $this->title . "',
                                address='". $this->address ."'
                               where id='" . $this->id . "' limit 1";
        }else {
            $query = "insert into shops values(null,
                                                '" . $this->title . "',
                                                '" . $this->address . "'
                                               )";
        }
       $result =  mysqli_query($this->conn,$query);

        if(!$result) {
            throw new Exception(mysqli_error($this->conn),400);

        }
    }
    public function all() {
        $result = mysqli_query($this->conn,"select * from shops order by id desc");
        return mysqli_fetch_all($result,MYSQLI_ASSOC);
    }
    public function getById($id) {
        $result = mysqli_query($this->conn,"select * from shops where id = '$id' limit 1 ");
        $one = mysqli_fetch_all($result,MYSQLI_ASSOC);
        $one = reset($one);
        return $one;
    }
    public function deleteById($id){
        mysqli_query($this->conn,"delete from shops where id = '$id' limit 1 ");
    }
}