<?php
include_once __DIR__."/../Service/DBConnector.php";
include_once  __DIR__."/../../../common/src/Service/ExceptionService.php";

class BasketItem
{
    public $id;
    public $basketId;
    public $productId;
    public $quantity;


    private $conn;

    public function __construct($basketId = null,$productId = null, $quantity = null)
    {
        $this->conn =DBConnector::getInstance()->connect();
        $this->basketId = $basketId;
        $this->productId = $productId;
        $this->quantity = $quantity;


    }
    public function save(){
        $query = "insert into basket_item values(null, '" . $this->basketId . "', '" . $this->productId . "','" . $this->quantity . "')";

        $result= mysqli_query($this->conn,$query);

        if (!$result) {
            throw new Exception(mysqli_error($this->conn));
        }
    }

    public function update(){
        if (empty($this->basketId) || empty($this->productId) || empty($this->quantity)) {
           throw new Exception("Empty basket item field");

        }
            $query = "update basket_item set quantity = " . $this->quantity . " where basket_id = " . $this->basketId
                . " AND product_id = " . $this->productId
                . " LIMIT 1";
        $result= mysqli_query($this->conn,$query);
        if (!$result) {
            throw new Exception(mysqli_error($this->conn));
        }
    }

    public function getByBasketId($basketId) {
        $result = mysqli_query($this->conn,"select * from basket_item where basket_id = '$basketId'");
//        print_r("select * from basket_item where basket_id = '$basketId'");
//        die();
        $oneProduct = mysqli_fetch_all($result,MYSQLI_ASSOC);
//        print_r($oneProduct);
//        die();
        return $oneProduct;
    }
    public function deleteProductByBasketId($basketId,$productId){
        mysqli_query($this->conn,"delete from basket_item where product_id = '$productId'and basket_id = '$basketId' limit 1 ");
    }
    public function ClearByBasketId($basketId){
        mysqli_query($this->conn,"delete from basket_item where basket_id = '$basketId'");
    }
}