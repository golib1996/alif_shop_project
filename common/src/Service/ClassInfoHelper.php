<?php


class ClassInfoHelper
{

    public static function classCheck($object) {

        if(file_exists(__DIR__."/../Model/" . $object.'.php')){
            return include_once __DIR__."/../Model/" . $object.'.php';

        }elseif(file_exists(__DIR__."/../Service/" . $object.'.php')){
            return include_once __DIR__."/../Service/" . $object.'.php';

        }elseif(file_exists(__DIR__."/../../../backend/src/Controller/" . $object.'.php')) {
            return include_once __DIR__."/../../../backend/src/Controller/" . $object.'.php';

        }elseif(file_exists(__DIR__."/../../../frontend/src/Controller/" . $object.'.php')) {
            return include_once __DIR__."/../../../frontend/src/Controller/" . $object.'.php';

        }else
            die('Такой класс не существует');
    }

    public static function getMethods($object) {

        self::classCheck($object);

        $objReflection = new ReflectionClass($object);
        $methods = $objReflection->getMethods();

        $methodsName = [];

        foreach ($methods as $method) {
            $methodsName[] = $method->getName();
        }

        return $methodsName;
    }

    public static function getVariables($object)
    {
        self::classCheck($object);

        $objReflection = new ReflectionClass($object);
        $variables = $objReflection->getProperties();

        $variablesName = [];

        foreach ($variables as $variable) {
            $variablesName[] = $variable->getName();
        }

        return $variablesName;
    }

    public static function getOperation($operation,$object) {

        if ($operation == 'variables') {
            return self::getVariables($object);

        }elseif ($operation == 'methods') {
            return self::getMethods($object);

        }else
            die('Неправильно введена операция');
    }

}