<?php
include_once __DIR__."/../Model/Basket.php";
include_once __DIR__."/../Model/BasketItem.php";

include_once __DIR__."/../Service/BasketService.php";

class BasketSessionService extends BasketService
{

    public function getBasketProducts($basketId) {
      $session = $_SESSION['basket'] ?? [];
      if (empty($session) && sizeof($session) == 0) {
          return $session;
      }
      return unserialize($session);
    }


    public  static  function getBasketByUSerId($userId) {

    }



    public function updateBasketItem($basketId,$productId,$qty) {
        $session = $this->getBasketProducts($basketId);


        foreach ($session as $key =>$item) {
            if ($item['product_id'] === $productId){
                $session[$key]['quantity'] = $qty;
            }

        }
        $this->save($session);


    }

    public function deleteBasketItem($basketId,$productId) {
        $session = $this->getBasketProducts($basketId);

        foreach ($session as $key =>$item) {
            if ($item['product_id'] === $productId){
                unset($session[$key]);
            }
        }
        $this->save($session);
    }

    public function createBasketItem($basketId,$productId,$qty) {
       $item = [
           'product_id'=>$productId,
           'basket_id'=>$basketId,
           'quantity'=>$qty
        ];

         $session = $this->getBasketProducts($basketId);
         $session[] = $item;

        $this->save($session);

    }

    public function save($session) {
        $_SESSION['basket'] =serialize($session);
    }

    public function clearBasket($basketId)
    {
        $session = $this->getBasketProducts($basketId);

        foreach ($session as $key =>$item) {

                unset($session[$key]);
            }

        $this->save($session);


    }

    public function getBasketIdByUserId($userId)
    {


    }
}