<?php


class DataHelper
{
    public static function saveArrayToCsvFile($array2x,$file) {

        $fp = fopen($file,'w');

        foreach ($array2x as $value) {
            fputcsv($fp,$value);
        }
        fclose($fp);
    }

    public static function getArrayToCsvFile($file) {

        $csv = array_map('str_getcsv', file($file));

        return $csv;
    }
}

