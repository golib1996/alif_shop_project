<?php


class Validator
{
    public static function emailValidate($email) {
        if(preg_match('/\b[\w\.-]+@[\w\.-]+\.\w{2,4}\b/', $email)) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public static function phoneValidate($phone) {
        if(preg_match('/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', $phone)) {
            return 'true';
        } else {
            return 'false';
        }
    }
}