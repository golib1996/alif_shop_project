<?php
include_once __DIR__."/../Model/Basket.php";
include_once __DIR__."/../Model/BasketItem.php";
include_once __DIR__."/Interfaces/BasketInterface.php";
include_once __DIR__."/../Service/BasketService.php";

class BasketDBService extends BasketService
{
    public function getBasketProducts($basketId) {
        return (new BasketItem())->getByBasketId($basketId);
    }


    public  static  function getBasketByUSerId($userId) {
        $basket = new Basket($userId);
        if ($basket->getFromDB() == null) {
            $basket->userId = $userId;
            $basket->save();
        }
        return $basket->getFromDB();
    }

    public function updateBasketItem($basketId,$productId,$qty) {
        (new BasketItem($basketId,$productId,$qty))->update();
    }

    public function deleteBasketItem($basketId,$productId) {
        (new BasketItem())->deleteProductByBasketId($basketId,$productId);
    }

    public function createBasketItem($basketId,$productId,$qty) {
        $item = new BasketItem();
        $item->basketId=$basketId;
        $item->productId = $productId;
        $item->quantity = $qty;

        $item->save();
    }

    public function clearBasket($basketId)
    {
        (new BasketItem())->ClearByBasketId($basketId);
    }

    public function getBasketIdByUserId($userId)
    {
        return (new Basket($userId))->getFromDB()['id'];
    }
}