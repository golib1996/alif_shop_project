<?php
include_once __DIR__."/../../../common/src/Service/ExceptionService.php";
include_once __DIR__."/../../../common/src/Service/UserService.php";
include_once __DIR__."/../../../common/src/Service/OrderService.php";
include_once __DIR__."/../../../common/src/Service/BasketService.php";
include_once __DIR__."/../../../common/src/Service/BasketDBService.php";
include_once __DIR__."/../../../common/src/Service/BasketSessionService.php";
include_once __DIR__."/../../../common/src/Service/BasketCookieService.php";
include_once __DIR__."/../../../common/src/Model/Order.php";
include_once __DIR__."/../../../common/src/Model/OrderItem.php";

class OrderController
{
    private $basketServer;

    public function  __construct()
    {
        //TODO check BasketService with BasketController
       // $this->basketServer = new BasketSessionService();
        //$this->basketServer = new BasketCookieService();
        $this->basketServer = new BasketDBService();
    }

    public function index() {
        include_once __DIR__."/../../views/order/form.php";
    }

    public function create() {
        $name = htmlspecialchars($_POST['name']);
        $phone = htmlspecialchars($_POST['phone']);
        $email = htmlspecialchars($_POST['email']);
        $delivery = (int)$_POST['delivery'];
        $payment = (int)$_POST['payment'];
        $comment = htmlspecialchars($_POST['comment']);
        $userId = UserService::getCurrentUser()['id'] ?? 0;
        $total = 0;
        $status = OrderService::STATUS_NEW;
        $updated = date('Y-m-d H:i:s',time());
        $order = new Order(null,$userId,$payment,$delivery,$total,$name,$phone,$email,$comment,$status,$updated);

        $orderId =$order->save();


        if(empty($orderId)) {
            throw new Exception("Order ID is null",400);

        }

        $basketId = $this->basketServer->getBasketIdByUserId($userId);
        $items = $this->basketServer->getBasketProducts($basketId);



        if(empty($items)) {
            throw new Exception("Basket is empty",400);

        }

        foreach ($items as $item) {
            $orderItem = new OrderItem($orderId,(int)$item['product_id'],(int)$item['quantity']);
            $orderItem->save();
        }
        // clear basket

        $this->basketServer->clearBasket($basketId);

        header("Location: /?model=order&action=success&order_id=". $orderId);

    }
    public function success(){
     $orderId = (int)$_GET['order_id'];
     include_once __DIR__."/../../views/order/sucess.php";
    }
}