<?php

include_once  __DIR__."/../../../common/src/Model/Product.php";
include_once  __DIR__."/../../../common/src/Service/ExceptionService.php";

class ProductController
{

    public function all(){

        $categories =isset($_GET['category_id']) ? explode(',',$_GET['category_id']) : [];

        $limit = intval($_GET['limit'] ?? Product::NUMBER_PRODUCTS_PER_PAGE);

        $currentPage = $_GET['page'];
        $allPageNumber = (new Product())->getNumberPage($_GET['category_id'] ?? [],
            $_GET['limit'] ?? Product::NUMBER_PRODUCTS_PER_PAGE);

        try {
            if (empty($currentPage))
                $currentPage = 1;
            if ($currentPage <=0 || $currentPage > $allPageNumber) {
                throw new Exception("This Page is not exists", 400);
            }

            $offset = (intval($currentPage ?? 1) - 1) * $limit;
            $offset = $offset < 0 ? 0 : $offset;

            $all = (new Product())->all($categories, $limit, $offset);
            include_once __DIR__ . "/../../views/product/list.php";
        }catch (Exception $e) {
            ExceptionService::error($e,'frontend');
        }
    }

    public function view()
    {
        try {
            if (!isset($_GET['id'])) {
                throw new Exception("ID is not exists", 400);
            }
            $id = (int)$_GET['id'];
            if (empty($id)) {
                throw new Exception("ID is empty", 400);
            }
            $oneProduct = (new Product())->getById($id);
            if (empty($oneProduct)) {
                throw new Exception("Product not found", 404);
            }
            include_once __DIR__ . "/../../views/product/view.php";
        } catch (Exception $e) {
           ExceptionService::error($e,'frontend');
        }
    }
}