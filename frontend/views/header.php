
<?php
include_once __DIR__."/../../common/src/Service/UserService.php";
include_once __DIR__."/../../common/src/Service/BasketService.php";
include_once __DIR__."/../../common/src/Service/BasketDBService.php";
include_once __DIR__."/../../common/src/Service/CategoryService.php";
include_once __DIR__."/../../common/src/Service/ProductService.php";

$currentUser = UserService::getCurrentUser();
$basketDetails = (new ProductService())->
                    getBasketItems(( new BasketDBService())->
                                                    getBasketProducts((UserService::getCurrentUser())['id']));
$items = $basketDetails['items'];
if (!empty($items)) {
  $quantity = sizeof($items);
  $total = $basketDetails['total'];
    }
else {
    $quantity = 0;
    $total = 0;
}


    ?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Shop</title>
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/shop-style.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="/js/scripts.js"></script>
</head>
<body>


<header>
    <div id="head">
        <div class="top">
            <div class="width1024">
                <ul class="desktop-element">
                    <li><?=!empty($currentUser['login']) ? '<span style="color: #fff">Hello,'.$currentUser['login'].'!</span>' : '<a href="/?model=register&action=form">Register</a>' ?></li>
                    <li><?=!empty($currentUser['login']) ?  '<a href="/?model=auth&action=logout">Sign out</a>': '<a href="/?model=site&action=login">Sign in</a>'?></li>
                   <?=!empty($currentUser['login']) ? '<li><a href="/?model=basket&action=view">Basket</a></li>': ''?>
                    <li><a href="">Help</a></li
                </ul>
                <div id="mobile-logo" class="mobile-element">BOOKS</div>
                <select id="top-link" onchange="document.location=this.value" class="mobile-element">
                    <option disable selected></option>
                    <option value="/?model=site&action=login">Sign in</option>
                    <option value="/?model=register&action=form">Register</option>
                    <option value="#order">Order Status</option>
                    <option value="#help">Help</option>
                </select>
            </div>
        </div>
        <div class="header-panel">
            <div class="width1024 flex">
                <div id="logo">
                    <a href="/"><img src="/imgs/logo.png"></a>
                </div>
                <div id="search-field">
                    <form action="#" >
                        <input type="text" name="search_text">
                        <button>Search</button>
                    </form>
                </div>
                <div id="basket-container">
                    <div>Your cart <span>(<?=$quantity?> items)</span></div>
                    <div><b>$<?=$total?></b><a href="#">Checkout</a></div>
                </div>
                <div id="favor">
                    <div> Wish List </div>
                </div>
            </div>
        </div>
    </div>
    <nav>
        <ul  class="width1024 desktop-element">
            <?php foreach (CategoryService::getGenre() as $genre): ?>
            <li><a href="/?model=product&action=all&category_id=<?=$genre['id']?>&page=1"><?=$genre['title']?></a></li>
            <?php endforeach; ?>
        </ul>

        <select onchange="document.location=this.value" class="mobile-element">
            <option disabled selected></option>
            <option value="#Computers">Computers</option>
            <option value="#Cooking">Cooking</option>
            <option value="#">Educations</option>
            <option value="#">Functions</option>
            <option value="#">Health</option>
            <option value="#">Mathematics</option>
            <option value="#">Medical</option>
            <option value="#">Reference</option>
            <option value="#">Science</option>
        </select>
    </nav>
</header>

<div class="body">
