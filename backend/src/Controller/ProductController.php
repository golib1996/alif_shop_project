<?php
include_once __DIR__ . "/AbstractController.php";
include_once  __DIR__."/../../../common/src/Model/Product.php";
include_once __DIR__."/../../../common/src/Service/FileUploader.php";
include_once __DIR__."/../../../common/src/Service/UserService.php";
include_once __DIR__."/../../../common/src/Service/ValidationService.php";
include_once __DIR__."/../../../common/src/Service/ProductValidator.php";

class ProductController extends AbstractController
{

    public function create() {
        include_once  __DIR__ . "/../../views/product/form.php";
    }
    public function update($id = null) {
        $id = !empty($id) ? $id : (int)$_GET['id'];
        if (empty($id)) die ('Undefined ID');
        $oneProduct = (new Product())->getById($id);
        if (empty($oneProduct)) die ('Product not found');
        include_once  __DIR__ . "/../../views/product/form.php";
    }
    public function read() {
        $all =(new Product())->all();
        include_once  __DIR__ . "/../../views/product/list.php";
    }
    public function delete()
    {
        $id = (int)$_GET['id'];
        if (empty($id)) die ('Undefined ID');
        (new Product())->deleteById($id);
        return $this->read();
    }

    public function save()
    {
        if(!empty($_POST)) {
            $filename=FileUploader::upload('products');
            $now = date('Y-m-d H:i:s', time());

            if(!ProductValidator::validate()) {
                    return !empty($_POST['id']) ? $this->update($_POST['id']) : $this->create();
            }

            $product = new Product(intval($_POST['id']),
                htmlspecialchars($_POST['title']),
                htmlspecialchars($filename ?? ''),
                htmlspecialchars($_POST['preview']),
                htmlspecialchars($_POST['content']),
                (int)$_POST['price'],
                (int)$_POST['status'],
                $now,
                $now);
            $product->save();
        }
        return $this->read();
    }

    public function all(){

        $categories =isset($_GET['category_id']) ? explode(',',$_GET['category_id']) : [];

        $limit = intval($_GET['limit'] ?? Product::NUMBER_PRODUCTS_PER_PAGE);

        $currentPage = $_GET['page'];
        $allPageNumber = (new Product())->getNumberPage($_GET['category_id'] ?? [],
            $_GET['limit'] ?? Product::NUMBER_PRODUCTS_PER_PAGE);

        try {
            if (empty($currentPage))
                $currentPage = 1;
            if ($currentPage <= 0 || $currentPage > $allPageNumber) {
                throw new Exception("This Page is not exists", 400);
            }

            $offset = (intval($currentPage ?? 1) - 1) * $limit;
            $offset = $offset < 0 ? 0 : $offset;

            $all = (new Product())->all($categories, $limit, $offset);
            include_once __DIR__ . "/../../views/product/list.php";
        }catch (Exception $e) {
            ExceptionService::error($e,'backend');
        }
    }

}
