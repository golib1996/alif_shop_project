<?php
include_once __DIR__ . "/AbstractController.php";
include_once  __DIR__."/../../../common/src/Model/Category.php";
include_once __DIR__."/../../../common/src/Service/UserService.php";

class CategoryController extends AbstractController
{


    public function create() {
        include_once  __DIR__ . "/../../views/category/form.php";
    }
    public function update() {
        $id =(int)$_GET['id'];
        if (empty($id)) die ('Undefined ID');
        $one = (new Category())->getById($id);
        if (empty($one)) die ('Product not found');
        include_once  __DIR__ . "/../../views/category/form.php";
    }
    public function read() {
        $all =(new Category())->all();
        //   include_once  __DIR__ . "/../../views/product/all.php";
        include_once  __DIR__ . "/../../views/category/list.php";
    }
    public function delete()
    {
        $id = (int)$_GET['id'];
        if (empty($id)) die ('Undefined ID');
        (new Category())->deleteById($id);
        return $this->read();
    }

    public function save()
    {
        if(!empty($_POST)) {

            $category = new Category(intval($_POST['id']),
                htmlspecialchars($_POST['title']),
                (int)$_POST['group_id'],
                (int)$_POST['parent_id'],
                (int)$_POST['prior']
                );
            $category->save();
        }
        return $this->read();
    }

}