<?php
include_once __DIR__ . "/AbstractController.php";
include_once  __DIR__."/../../../common/src/Model/Order.php";
include_once  __DIR__."/../../../common/src/Model/OrderItem.php";
include_once  __DIR__."/../../../common/src/Service/OrderService.php";
include_once __DIR__."/../../../common/src/Service/UserService.php";

class OrderController extends AbstractController
{


    public function create() {

        include_once  __DIR__ . "/../../views/orders/form.php";
    }
    public function update()
    {


        if(!empty($_POST)) {

            $id = (int)$_POST['id'];
            $delivery = (int)$_POST['delivery'];
            $payment = (int)$_POST['payment'];
            $name = htmlspecialchars($_POST['name']);
            $phone = htmlspecialchars($_POST['phone']);
            $email = htmlspecialchars($_POST['email']);
            $status = (int)$_POST['status'];
            $updated = date('Y-m-d H:i:s',time());
           if($id >0) {
               (new Order($id, null, $payment, $delivery, 0, $name, $phone, $email, "", $status, $updated))->update();
           }
           header("Location: /?model=order&action=read");
        }



        $one = (new Order())->getById((int)$_GET['id']);
        include_once  __DIR__ . "/../../views/orders/form.php";

    }
    public function read() {
        $all =(new Order())->all();
        //   include_once  __DIR__ . "/../../views/product/all.php";
        include_once  __DIR__ . "/../../views/orders/list.php";
    }


    public function save()
    {
        //TODO
    }
    public function delete(){
        //todo
    }

}