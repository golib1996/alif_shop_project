<?php
include_once __DIR__ . "/AbstractController.php";
include_once  __DIR__."/../../../common/src/Model/Permission.php";
include_once __DIR__."/../../../common/src/Service/UserService.php";

class PermissionController extends AbstractController
{


    public function create() {
        include_once  __DIR__ . "/../../views/permission/form.php";
    }

    public function read() {
        $all =(new Permission())->all();
        include_once  __DIR__ . "/../../views/permission/list.php";
    }

    public function update() {
        //TODO: Implement update method.
    }

    public function delete()
    {
        $name = htmlspecialchars($_GET['permission']);
        (new Permission())->deleteByName($name);
        header("Location: /?model=permission&action=read");
    }

    public function save()
    {
        if(!empty($_POST)) {
            $news = new Permission(htmlspecialchars($_POST['permission']));
            $news->save();
        }
        return $this->read();
    }

}