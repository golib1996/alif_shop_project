<?php
include_once __DIR__ . "/AbstractController.php";
include_once  __DIR__."/../../../common/src/Model/Payment.php";

class PaymentController extends AbstractController
{

    public function create() {
        include_once  __DIR__ . "/../../views/payment/form.php";
    }
    public function update()
    {


        if(!empty($_POST)) {

            $id = (int)$_POST['id'];
            $title = htmlspecialchars($_POST['title']);
            $code = htmlspecialchars($_POST['code']);
            $priority = (int)($_POST['priority']);
            if($id >0) {
                (new Payment($id, $title,$code,$priority))->save();
            }
            header("Location:/?model=payment&action=read");
        }



        $one = (new Payment())->getById((int)$_GET['id']);
        include_once  __DIR__ . "/../../views/payment/form.php";

    }
    public function read() {
        $all =(new Payment())->all();
        //   include_once  __DIR__ . "/../../views/payment/all.php";
        include_once  __DIR__ . "/../../views/payment/list.php";
    }
    public function delete()
    {
        $id = (int)$_GET['id'];
        if (empty($id)) die ('Undefined ID');
        (new Payment())->deleteById($id);
        return $this->read();
    }

    public function save()
    {
        $payment = new Payment(intval($_POST['id']),
            htmlspecialchars($_POST['title']),
            htmlspecialchars($_POST['code']),
            (int)$_POST['priority']);
        $payment->save();

        return $this->read();
    }

}