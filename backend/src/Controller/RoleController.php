<?php
include_once __DIR__ . "/AbstractController.php";
include_once  __DIR__."/../../../common/src/Model/Role.php";
include_once __DIR__."/../../../common/src/Service/UserService.php";

class RoleController extends AbstractController
{


    public function create() {
        include_once  __DIR__ . "/../../views/role/form.php";
    }

    public function read() {
        $all =(new Role())->all();
        include_once  __DIR__ . "/../../views/role/list.php";
    }

    public function update() {
        //TODO: Implement update method.
    }

    public function delete()
    {
        $name = htmlspecialchars($_GET['role']);
        (new Role())->deleteByName($name);
        header("Location: /?model=role&action=read");
    }

    public function save()
    {
        if(!empty($_POST)) {
            $news = new Role(htmlspecialchars($_POST['role']));
            $news->save();
        }
        return $this->read();
    }

}