<?php
include_once __DIR__."/../../../common/src/Service/DBConnector.php";

class Fixture01
{
    private $conn;
    private $data = [
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'picture'=>'01.jpg',
            'preview'=>'1',
            'content'=>'golib',
            'price'=>'11111',
            'status'=>'23',
            'created'=>'2021-02-03 12:27:04',
            'updated'=>'2021-02-04 14:15:55'
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд2',
            'picture'=>'02.jpg',
            'preview'=>'1',
            'content'=>'golib',
            'price'=>'11111',
            'status'=>'23',
            'created'=>'2021-02-03 12:27:04',
            'updated'=>'2021-02-04 14:15:55'
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд3',
            'picture'=>'03.jpg',
            'preview'=>'1',
            'content'=>'golib',
            'price'=>'11111',
            'status'=>'23',
            'created'=>'2021-02-03 12:27:04',
            'updated'=>'2021-02-04 14:15:55'
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд4',
            'picture'=>'04.jpg',
            'preview'=>'1',
            'content'=>'golib',
            'price'=>'11111',
            'status'=>'23',
            'created'=>'2021-02-03 12:27:04',
            'updated'=>'2021-02-04 14:15:55'
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд5',
            'picture'=>'05.jpg',
            'preview'=>'1',
            'content'=>'golib',
            'price'=>'11111',
            'status'=>'23',
            'created'=>'2021-02-03 12:27:04',
            'updated'=>'2021-02-04 14:15:55'
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'picture'=>'01.jpg',
            'preview'=>'1',
            'content'=>'golib',
            'price'=>'11111',
            'status'=>'23',
            'created'=>'2021-02-03 12:27:04',
            'updated'=>'2021-02-04 14:15:55'
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд2',
            'picture'=>'02.jpg',
            'preview'=>'1',
            'content'=>'golib',
            'price'=>'11111',
            'status'=>'23',
            'created'=>'2021-02-03 12:27:04',
            'updated'=>'2021-02-04 14:15:55'
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд3',
            'picture'=>'03.jpg',
            'preview'=>'1',
            'content'=>'golib',
            'price'=>'11111',
            'status'=>'23',
            'created'=>'2021-02-03 12:27:04',
            'updated'=>'2021-02-04 14:15:55'
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд4',
            'picture'=>'04.jpg',
            'preview'=>'1',
            'content'=>'golib',
            'price'=>'11111',
            'status'=>'23',
            'created'=>'2021-02-03 12:27:04',
            'updated'=>'2021-02-04 14:15:55'
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд5',
            'picture'=>'05.jpg',
            'preview'=>'1',
            'content'=>'golib',
            'price'=>'11111',
            'status'=>'23',
            'created'=>'2021-02-03 12:27:04',
            'updated'=>'2021-02-04 14:15:55'
        ],
    ];
    public function __construct(DBConnector $conn)
    {
        $this->conn = $conn->connect();
    }

    public function run(){
        foreach ($this->data as $product) {
            copy(__DIR__ . "/../../fixtures_pics/" . $product['picture'], __DIR__ . "/../../../uploads/products/" . $product['picture']);
            $result = mysqli_query($this->conn, "INSERT INTO products VALUES (".$product['id'].", 
                                                                                    '".$product['title']."',	
                                                                                    '".$product['picture']."',	
                                                                                    '".$product['preview']."',	
                                                                                    '".$product['content']."',	
                                                                                    '".$product['price']."',	
                                                                                    '".$product['status']."',	
                                                                                    '".$product['created']."',	
                                                                                    '".$product['updated']."')");
            if (!$result) {
                print mysqli_error($this->conn) . PHP_EOL;
            }
        }

    }
}