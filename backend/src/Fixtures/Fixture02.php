<?php
include_once __DIR__."/../../../common/src/Service/DBConnector.php";

class Fixture02
{
    private $conn;
    private $data = [
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'group_id'=>'1',
            'parent_id'=>'2',
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'group_id'=>'1',
            'parent_id'=>'2',
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'group_id'=>'1',
            'parent_id'=>'2',
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'group_id'=>'1',
            'parent_id'=>'2',
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'group_id'=>'1',
            'parent_id'=>'2',
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'group_id'=>'1',
            'parent_id'=>'2',
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'group_id'=>'1',
            'parent_id'=>'2',
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'group_id'=>'1',
            'parent_id'=>'2',
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'group_id'=>'1',
            'parent_id'=>'2',
        ],
        [
            'id'=>'null',
            'title'=>'Генри Форд1',
            'group_id'=>'1',
            'parent_id'=>'2',
        ]
    ];
    public function __construct(DBConnector $conn)
    {
        $this->conn = $conn->connect();
    }

    public function run(){
        foreach ($this->data as $category) {
            $result = mysqli_query($this->conn, "INSERT INTO categories VALUES (".$category['id'].", 
                                                                                    '".$category['title']."',	
                                                                                    '".$category['group_id']."',	
                                                                                    '".$category['parent_id']."')");
            if (!$result) {
                print mysqli_error($this->conn) . PHP_EOL;
            }
        }

    }
}