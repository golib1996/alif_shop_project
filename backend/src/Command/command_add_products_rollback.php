<?php

include_once __DIR__ . "/../Migrations/202001161512_migration_add_products.php";
include_once __DIR__ . "/../../../common/src/Service/DBConnector.php";

$dbConnector = DBConnector::getInstance();
$migration = new MigrationAddProducts($dbConnector);
$migration->rollback();

die('OK');