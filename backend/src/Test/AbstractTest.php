<?php


abstract class AbstractTest
{
    protected $conn;

    const DB_PRODUCT_NAME = 'db_shop';
    const DB_TEST_NAME = 'shop_test';

    public function __construct()
    {
        $this->conn = (new DBConnector(
            'localhost',
            'shop_test_user',
            'shop_test_password',
            'shop_test'))->connect();


    }

    public function copyTableByName($name) {
        $query = "show create table". self::DB_PRODUCT_NAME.".".$name;

        $result = mysqli_query($this->conn, $query) or die(mysqli_error($this->conn)) ;

        $result = mysqli_fetch_all($result,MYSQLI_ASSOC);


        mysqli_query($this->conn, $result[0]['Create table']);
        mysqli_query($this->conn, "truncate table ". self::DB_TEST_NAME.".".$name);

        mysqli_query($this->conn, "insert into ". self::DB_TEST_NAME. ".".$name
        . " select * from ".self::DB_PRODUCT_NAME.".".$name);

    }

    public function dropTableByName($name) {
        mysqli_query($this->conn, "drop table".self::DB_TEST_NAME. ".".$name);
    }

}