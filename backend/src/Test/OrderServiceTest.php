<?php

include_once __DIR__."/../../../common/src/Model/Order.php";
include_once __DIR__."/../../../common/src/Service/OrderService.php";
include_once __DIR__."/../../../backend/src/Test/AbstractTest.php";
class OrderServiceTest extends AbstractTest
{

    public  function testCalcTotal()
    {
        $this->copyTableByName('products');
        $this->copyTableByName('orders');
        $this->copyTableByName('order_item');

        $orderService = new OrderService();

        $quantityAndProducts = (new Order())->getProductsAndQuantityByOrderId(20);

        if (!method_exists($orderService,'calcTotal')) {
            print "Error:calcTotal() is not exists".PHP_EOL;
            die('TEST WAS CRASHED');
        }

        $total = $orderService->calcTotal($quantityAndProducts);

        if (277775 != $total) {
            print "Error:calcTotal() is not exists".PHP_EOL;
            die('TEST WAS CRASHED');
        }

        $this->dropTableByName('products');
        $this->dropTableByName('orders');
        $this->dropTableByName('order_item');
    }


}