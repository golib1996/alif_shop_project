<?php include_once __DIR__."/../header.php"; ?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Categories</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Categories</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">

            <div class="card-body p-0">
                <table class="table table-striped projects">
                    <thead>
                    <th>ID</th>
                    <th>Title</th>
                    <th>GroupId</th>
                    <th>ParentId</th>
                    <th>Prior</th>
                    </thead>
                    <tbody>
                    <?php foreach ($all as $category): ?>
                        <tr>
                            <td><?=$category['id']?></td>
                            <td><?=$category['title']?></td>
                            <td><?=$category['group_id']?></td>
                            <td><?=$category['parent_id']?></td>
                            <td><?=$category['prior']?></td>

                            <td style="width: 200px;">
                                <!--                                    <a-- class="btn btn-primary btn-sm" href="#">-->
                                <!--                                        <i class="fas fa-folder">-->
                                <!--                                        </i>-->
                                <!--                                        View-->
                                <!--                                    </a-->
                                <a class="btn btn-info btn-sm" href="/?model=category&action=update&id=<?=$category['id']?>">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                    Edit
                                </a>
                                <a class="btn btn-danger btn-sm" href="/?model=category&action=delete&id=<?=$category['id']?>">
                                    <i class="fas fa-trash">
                                    </i>
                                    Delete
                                </a>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php include_once __DIR__."/../footer.php"; ?>
