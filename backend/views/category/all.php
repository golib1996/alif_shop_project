<?php
include_once __DIR__."/../header.php";
?>

    <div>
        <a class="btn btn-warning" href="/?model=category&action=create">Добавить категорию</a>
    </div>

    <table class="table">
        <thead>
        <th>ID</th>
        <th>Title</th>
        <th>GroupId</th>
        <th>ParentId</th>
        </thead>
        <tbody>
        <?php foreach ($all as $category): ?>
            <tr>
                <td><?=$category['id']?></td>
                <td><?=$category['title']?></td>
                <td><?=$category['group_id']?></td>
                <td><?=$category['parent_id']?></td>

                <td style="width: 200px;">
                    <a href="/?model=category&action=delete&id=<?=$category['id']?>" class="btn btn-danger">Delete</a>
                    <a href="/?model=category&action=update&id=<?=$category['id']?>" class="btn btn-warning">Update</a>
                </td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
<?php
include_once __DIR__."/../footer.php";
?>