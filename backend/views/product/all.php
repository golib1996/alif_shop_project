<?php
include_once __DIR__."/../header.php";
?>

<div>
    <a class="btn btn-warning" href="/?model=product&action=create">Добавить товар</a>
</div>

<table class="table">
                <thead>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Picture</th>
                    <th>Preview</th>
                    <th>Content</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th>Actions</th>
                </thead>
                <tbody>
                <?php foreach ($all as $product): ?>
                <tr>
                    <td><?=$product['id']?></td>
                    <td><?=$product['title']?></td>
                    <td>
                        <img src="/shop/uploads/products/<?=$product['picture']?>" alt="test">
                    </td>
                    <td><?=$product['preview']?></td>
                    <td><?=$product['content']?></td>
                    <td><?=$product['price']?></td>
                    <td><?=$product['status']?></td>
                    <td><?=$product['created']?></td>
                    <td><?=$product['updated']?></td>
                    <td style="width: 200px;">
                        <a href="/?model=product&action=delete&id=<?=$product['id']?>" class="btn btn-danger">Delete</a>
                        <a href="/?model=product&action=update&id=<?=$product['id']?>" class="btn btn-warning">Update</a>
                    </td>
                </tr>
                <?php endforeach; ?>

                </tbody>
            </table>
<?php
include_once __DIR__."/../footer.php";
?>