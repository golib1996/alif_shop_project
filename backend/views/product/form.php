<?php
include_once __DIR__."/../header.php";
include_once __DIR__."/../../../common/src/Service/MessageService.php"
?>

<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Create Product</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active"> Create Product</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="content">
        <div class="card card-info">
            <?php
            $errorMessage = MessageService::displayError();
            if(isset($errorMessage)) : ?>
            <div class = "error"><?=$errorMessage ?></div>
            <?php endif; ?>
            <!-- /.card-header -->
            <!-- form start -->
            <form  class="form-horizontal" action="/?model=product&action=save" method="post" enctype="multipart/form-data">
                <div class="card-body">
                    <input type ="hidden" value="<?=$oneProduct['id'] ?? '' ?>" name = "id">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                            <input type="text" value="<?=$oneProduct['title'] ?? '' ?>" name="title" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Picture</label>
                        <div class="col-sm-10">
                            <input type="file" name="picture"  class="form-control">

                            <?php
                            if(!empty($oneProduct['picture'])) {
                                ?>
                                <img src="//shop/uploads/products/<?= $oneProduct['picture']?>" style=" width: 70px;">
                                <?php
                            }
                            ?>
                        </div>

                    </div>


                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Preview</label>
                        <div class="col-sm-10">
                            <input type="text" value="<?=$oneProduct['preview'] ?? '' ?>" name="preview" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Status</label>
                        <div class="col-sm-10">
                            <input type="text" value="<?=$oneProduct['status'] ?? '' ?>" name="status" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Price</label>
                        <div class="col-sm-10">
                            <input type="text" value="<?=$oneProduct['price'] ?? '' ?>" name="price" class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Content</label>
                        <div class="col-sm-10">
                            <textarea rows="7" name="content" class="form-control"><?=$oneProduct['content'] ?? '' ?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <input type="submit"  class="btn btn-success" value="Save">
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
<?php
include_once __DIR__."/../footer.php";
?>
